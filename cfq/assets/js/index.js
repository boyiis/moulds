

// var html2 = "<input type='button' class='zk' value='新生成的展开' />";
// 	$("#zkdiv").append(html2);
// $("#zkdiv").on("click",".zk",function(){
//     console.log("on 点击一次");
// });

$(function () {

    $("#add-btn-prescription").click(function () {
        addPrescription();
    });

    var i = 1;

    function addPrescription() {
        i++;

        var title = `<li>
        <a href="${'#tab' + i}" data-toggle="tab" id='${'tabTitle' + i}'>
            <span class="badge badge-count card-category">处方${i}
                <i class="la la-close tab-i-cursor"></i>
            </span>
        </a>
    </li>`;

        var content = `<div class="tab-pane fade" id="${'tab' + i}">
                            <p>${i}iOS 是一个由苹果公司开发和发布的手机操作系统。最初是于 2007 年首次发布 iPhone、iPod Touch 和 Apple TV。iOS 派生自 OS X，它们共享 Darwin 基础。OS X 操作系统是用在苹果电脑上，iOS
                                是苹果的移动版本。
                            </p>
                        </div>`;

        $("#prescription").append(title);
        $("#prescriptionContent").append(content);
        $('#tabTitle' + i).tab('show');
    }

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        // 获取已激活的标签页的名称
        var activeTab = $(e.target).text();
        // 获取前一个激活的标签页的名称
        var previousTab = $(e.relatedTarget).text();
        $(".active-tab span").html(activeTab);
        $(".previous-tab span").html(previousTab);
    });
});


